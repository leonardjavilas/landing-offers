$(document).ready(function () {

    //load google fonts asynchronously 
    // WebFontConfig = {
    //     google: {
    //         families: ['Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i', 'Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i']
    //     }
    // };
    // (function () {
    //     var wf = document.createElement('script');
    //     wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    //     wf.type = 'text/javascript';
    //     wf.async = 'true';
    //     var s = document.getElementsByTagName('script')[0];
    //     s.parentNode.insertBefore(wf, s);
    // })();

    //set of variables
    var $body = document.body;
    var $squeeze = document.getElementById('squeeze');
    var $burguerButton = document.getElementById('js-btn-burger');
    var $menuOffcanvas = document.getElementById('js-menu-offcanvas');
    var btnBackToTop = document.getElementById('btn-back-to-top');


    //set of constants
    const touchLeft = new Hammer($body);
    const touchRight = new Hammer($menuOffcanvas);

    //function set for Burger Menu and Menu
    function showMenu() {
        $menuOffcanvas.classList.add('open');
        $menuOffcanvas.classList.add('active');
        $squeeze.classList.add('is-active');
    };

    function hideMenu() {
        $menuOffcanvas.classList.remove('open');
        $menuOffcanvas.classList.remove('active');
        $squeeze.classList.remove('is-active');
    };

    function toggleMenuX() {
        $menuOffcanvas.classList.toggle('active');
        $squeeze.classList.toggle('is-active');
    };

    function toggleMenuBurger() {
        $('.offcanvas-collapse').toggleClass('open');
    };


    touchLeft.on('swipeleft', showMenu);
    touchRight.on('swiperight', hideMenu);
    $burguerButton.addEventListener('click', toggleMenuBurger);
    $burguerButton.addEventListener('click', toggleMenuX);


    //settings slider main
    $('.slider-main__box--items').slick({
        autoplay: false,
        autoplaySpeed: 3000,
        speed: 500,
        infinite: true,
        cssEase: 'linear',
        fade: true,
        arrows: true,
        dots: true,
        infinite: false,
        draggable: false,
        swipe: false,
        swipeToSlide: false,
        touchMove: false,
        //verticalSwiping: false
        //nextArrow: '<i class="fa fa-arrow-right"></i>',
        //prevArrow: '<i class="fa fa-arrow-left"></i>',
    });

    // event scroll for button back to top
    $(window).scroll(function () {
        if ($(window).scrollTop() > 300) {
            btnBackToTop.classList.add('show');
        } else {
            btnBackToTop.classList.remove('show');
        }
    });

    // event click for button back to top
    $('#btn-back-to-top').on('click', function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, '300');
    });









    // scrollspy
    $("body").scrollspy({
        target: '#js-menu-offcanvas'
    })

    //smooth Scroll
    $('a[href*="#"]:not([href="#"]):not([data-toggle])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });

    //fix scrollspy 
    $(window).on("scroll", function () {
        var scrollTop = $(window).scrollTop();
        if (scrollTop == 0 || scrollTop < 65) {
            $('.fixed-link').addClass('active');
        }
    });




});